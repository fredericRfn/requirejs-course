//File: scripts/main.js
require.config({   
    paths: {        
        "hello": "helper/world"
    },
});

requirejs(['hello'], function(hello) {
    var message = hello.getMessage();
    alert(message);
});
